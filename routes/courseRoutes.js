// accessing package of express
const express = require("express");
const router = express.Router(); /*use dot notation to access content of a package*/
const courseController = require("../controllers/courseController.js");
const auth = require("../auth.js");


//  s39 activity
// Create a single course
// route for creating a course
router.post("/create", auth.verify, (req, res) => {

const data = {
	course: req.body,
	isAdmin: auth.decode(req.headers.authorization).isAdmin
}

courseController.addCourse(data).then(resultFromController => res.send(resultFromController));
});
// Create a Single Course END


 /*end*/




// get all courses
router.get("/all", (req, res) =>{
	courseController.getAllCourse().then(resultFromController => res.send(resultFromController))
});

// get all active courses
router.get("/active", (req, res) =>{
	courseController.getActiveCourses().then(resultFromController => res.send(resultFromController))
});

router.get("/:courseId", (req, res) => {

						// retrives the id from the url
	courseController.getCourse(req.params.courseId).then(
		resultFromController => res.send(resultFromController))
});

// lclhst4000:/crs001/update - valid
// lchhst4000:/update/crs001 - error
router.patch("/:courseId/update", auth.verify, (req, res) =>
{
	const newData = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	courseController.updateCourse(req.params.courseId, newData).then(
		resultFromController => {
		res.send(resultFromController)
	});
});


// s40 Activity
// Archiving a single course

// router.patch("/:courseId/archive", auth.verify, (req, res) => {
//     const security ={
//         course: req.body,
//         isAdmin: auth.decode(req.headers.authorization).isAdmin
//     }
//     courseController.archiveCourse(req.params.courseId).then(resultFromController => {
//         res.send(resultFromController)
//     });
// });

router.patch("/:courseId/archive", auth.verify, (req,res) => {
	courseController.archiveCourse(req.params.courseId).then(resultFromController => {
		res.send(resultFromController)
	});
});

 router.post("/enroll", auth.verify, (req, res) => {
 	courseController.enroll(req.params.courseId).then(resultFromController => {
 		res.send(resultFromController)
 	})
 });


module.exports = router;