
// dependencies
const express = require("express");

const router = express.Router();

const auth = require("../auth.js");

const User = require("../models/user.js");

const userController = require("../controllers/userController.js");

router.post("/checkEmail", (req, res) => {
	userController.checkEmailExist(req.body).then(resultFromController => res.send(resultFromController))
});

router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});

router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(
		resultFromController => res.send(resultFromController))
});

router.post("/details", (req, res) => {
	userController.userDetails(req.body).then(
		resultFromController => res.send(resultFromController))
});

router.post("/archive", (req, res) => {
	userController.archiveCourse(req.body).then(
		resultFromController => res.send(resultFromController))
});

router.patch("/:userId/update", auth.verify, (req, res) =>
{
	const newData = {
		user: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	courseController.updateUser(req.params.courseId, newData).then(
		resultFromController => {
		res.send(resultFromController)
	})
});

// s41 activity
// Enroll a user

// route to enroll to a course
router.post("/enroll", auth.verify,(req, res) => {

	let data = {
		// User ID will be retrieved from the request header
		userId : req.body.userId,
		// course ID will be retrieved from the request body
		courseId : req.body.courseId
	};

	userController.enroll(data).then(
		resultFromController => res.send(resultFromController))
});






module.exports = router;