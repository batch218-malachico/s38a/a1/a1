// require model so we could use the model for searching
const User = require("../models/user.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");

module.exports.checkEmailExist = (reqBody) => {

	// ".find" - a mongoose crud operation (query) to find a field value from a collection
	return User.find({email:reqBody.email}).then(result => {
		// condition if there is an existing user
		if(result.length > 0){

			return true;

		}
		// condition if there is no existing user
		else
		{
				return false;
		}
	})
};


module.exports.registerUser = (reqBody) => {

	let newUser = new User ({
		firstName: reqBody.firstName,
		lastName:reqBody.lastName,
		email: reqBody.email,
		// bcrypt - package for password hashing
		// hashSync - synchronously generate a hash
		// hash - asynchronous
		password: bcrypt.hashSync(reqBody.password, 0),
		// 10 = salt rounds
		// Salt rounds is proportional to hashing rounds, the higher the salt rounds, the more hashing rounds.
		mobileNo: reqBody.mobileNo
	})

	return newUser.save().then((user, error) => {
		if(error){
			return false;
		}
		else{
			return true;
		}
	})

};

module.exports.loginUser = (reqBody) => {
	return User.findOne({email : reqBody.email}).then(result =>{
		if(result == null){
			return false;
		}
		else{
			// compareSync is bcrypt function to compare a unhashed password to hashed password
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result)};
			}
			else{
				// if password do not match
				return false;
				// return "Incorrect password"
			}
		}
	})
};

module.exports.userDetails = (userInfo) => {
	return User.findById(userInfo._id).then((details, err) => {
		if(err){
			return false;
		}
		else{
			details.password = "*****";
			return details;
		}
	})
};



module.exports.archiveCourse = (reqBody) => {
	return User.findOne({isAdmin: reqBody.isAdmin}).then((details, error) => {
		if(error){
			console.log(error);
			return false;
		}
		else{
			details.password = "";
			return details;
		}
	})
}


module.exports.updateUser = (userId, newData) => {
	if(newData.isAdmin == false) {
		return Course.findbyIdAndUpdate(userId, 
			{		/*req.body*/
				name: newData.course.name,
				description: newData.course.description,
				price: newData.course.price
			}
		).then((updateUser, error) => {
			if(error){
				return false
			}
			return true
		})

		}
		else{
			let message = Promise.resolve('User must be ADMIN to access this!')
			return message.then((value) => {return value})
	}
}


// Enroll user to a class
// user part
module.exports.enroll = async (data) => {

	let isUserUpdated = await User.findById(data.userId).then(user => {

		user.enrollments.push({courseId: data.courseId});

		return user.save().then((user, error) => {
			if(error) {
				return false;
			} else{
				return true;
			}
		})

	})

	let isCourseUpdated = await Course.findById(data.courseId).then(course => {

		course.enrollees.push({userId : data.userId});

		return course.save().then((course, error) => {
			if(error) {
				return false;
			} else {
				return true;
			}
		})
	})

	if(isUserUpdated && isCourseUpdated) {
		return true;
	} else {
		return false;
	}

};



